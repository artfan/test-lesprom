// task1
$(document).ready(function(){
	$(".main-form__select--one").on("change", function(){
		if ($(this).val() == 1){
			hideSelect2();
		} else {
			showSelect2();
			loadData();
		}
	});
	function showSelect2(){
		$(".main-form__label--two").addClass("main-form__label--two-show");
	}
	function hideSelect2(){
		$(".main-form__label--two").removeClass("main-form__label--two-show");
		$(".main-form__label--two option").remove();
	}
	function loadData(){
		$.ajax({
			url: "data.json",
			type: "get",
			data: "json",
			dataType: "html",
			beforeSend: function(){},
			success: function (data, textStatus){
				data = JSON.parse(data);
				$.each(data, function(index, value) {
					$('.main-form__select--two').append('<option value="' + value.value + '">'   + value.title + '</option>')
				}); 
			 },
			complete: function(){
			},
		})
	}
	$(".main-form").on("submit", function(e){
		e.preventDefault();
		if ($(".main-form__select--one").val()){
			return true;
		}
		$.ajax({
			url: "send.php",
			type: "post",
			beforeSend: function(){
				$(".btn").prop("disabled", true);
				console.log("start");
			},
			success: function(){
				console.log(true);
			},
			complete: function(){
				$(".btn").prop('disabled', false);
				console.log("end");

			},
		})
	});
});
// end task1

// task3
// выбор категории фильтров, подсветка активной категории
$(".list-filters__category").on("click", function(){
	$(this).addClass("active");
	$(this).siblings().removeClass("active");
	target = $(this).attr("data-category");
	$(target).addClass("show");
	$(target).siblings().removeClass("show");
});
// выбор фильтра и закрытие модаьного окна
$(".list-filters__filter-one").on("click", function(){
	target = $(this).attr("data-filter");
	$(target).addClass("show");
	$('.modal').modal('hide');
});
// закрытие открытого фильтра 
$(".filter__remove").on("click", function(){
	$(this).parent().removeClass("show");	
});
// end task3